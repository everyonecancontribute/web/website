/**
 * Convert numeric access levels to readable names.
 *
 * Taken from: https://docs.gitlab.com/ee/api/members.html#valid-access-levels
 *
 * @param {50|40|30|20|10|5} accessLevel
 * @returns
 */
export function humanizeAccessLevel(accessLevel) {
  switch (accessLevel) {
    case 50:
      return 'Owner'
    case 40:
      return 'Maintainer'
    case 30:
      return 'Developer'
    case 20:
      return 'Reporter'
    case 10:
      return 'Guest'
    default:
      console.error(`Unknown access level: ${accessLevel}`)
      return 'Unknown'
  }
}
