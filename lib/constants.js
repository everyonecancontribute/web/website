export const siteTitle = '#EveryoneCanContribute'

export const motto =
  'Everyone is welcome, everyone can contribute, everyone is unique and these are your strengths too!'
