import useSWR from 'swr'
import axios from 'axios'

const fetcher = (url) => axios.get(url)

export const fetch = (url) => useSWR(url, fetcher)?.data || {}
