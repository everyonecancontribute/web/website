/* eslint-disable filenames/match-regex */

module.exports = {
  ci: {
    collect: {
      startServerCommand: 'ENV=production yarn dev',
      url: ['http://localhost:3000'],
      settings: { chromeFlags: '--no-sandbox' },
    },
    upload: {
      target: 'temporary-public-storage',
    },
  },
}
