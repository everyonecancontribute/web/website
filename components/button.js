import Link from 'next/link'

const variants = {
  primary: 'bg-purple-700 hover:bg-purple-600 ring-purple-700',
}

export default function Button({ children, href, variant = 'primary' }) {
  return (
    <Link href={href} target="_blank">
      <a className={`text-white focus:ring-4 ring-opacity-30 p-2 rounded font-bold ${variants[variant]}`}>{children}</a>
    </Link>
  )
}
