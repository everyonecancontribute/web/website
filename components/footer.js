import Link from 'next/link'
import { motto, siteTitle } from '../lib/constants'
import { fetch } from '../lib/fetch'
import { Rss } from 'heroicons-react'

export default function Footer() {
  const { data: posts } = fetch('/api/posts')

  return (
    <footer className="bg-gray-900 text-white py-10">
      <div className="w-full md:w-3/4 grid sm-grid-cols-2 px-5 md:grid-cols-3 gap-4 m-auto">
        <div className="col-span-2">
          <span className="block text-purple-400 font-bold text-2xl mb-2">{siteTitle}</span>
          <span className="block sm:w-1/3 w-full">{motto}</span>
        </div>
        <div>
          <span className="flex items-center uppercase font-bold text-gray-200 tracking-wide mb-2">
            <Rss size={20} />
            <span className="ml-2">Recent Posts</span>
          </span>
          <ul>
            {posts
              ? posts.map((post) => (
                  <li className="text-purple-400 hover:underline focus:underline mb-1" key={post.id}>
                    <Link href={post.path}>{post.title}</Link>
                  </li>
                ))
              : ''}
          </ul>
        </div>
      </div>
    </footer>
  )
}
