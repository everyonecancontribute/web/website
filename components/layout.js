import Head from 'next/head'
import { motto, siteTitle } from '../lib/constants'
import Footer from './footer'
import Navbar from './navbar'
import { PageHeader } from './page-header'

export default function Layout({ children, home }) {
  return (
    <div>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <link rel="icon" type="image/png" href="/favicon.png" />
        <meta name="description" content={motto} />
        <meta name="og:title" content={siteTitle} />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <header>
        <Navbar />
      </header>
      <PageHeader />
      <main>{children}</main>
      <Footer />
    </div>
  )
}
