import { useRouter } from 'next/router'

export function PageHeader() {
  const router = useRouter()
  const useLargeHeader = shouldShowLargeHeader(router)

  const boxStyle = useLargeHeader ? 'p-16 py-20 lg:grid-cols-3' : 'py-5'
  const textStyle = useLargeHeader ? 'text-4xl' : 'hidden'

  return (
    <section className={`bg-gray-200 ${boxStyle} grid bg-cover`} style={{ backgroundImage: "url('/background.jpg')" }}>
      <span className={`text-white font-bold ${textStyle}`}>
        Everyone is welcome, everyone can contribute, everyone is unique and these are your strengths too!
      </span>
    </section>
  )
}

/**
 * Returns true if the page header should be large.
 *
 * @param {import('next/router').Router} router
 */
function shouldShowLargeHeader(router) {
  return router.pathname === '/'
}
