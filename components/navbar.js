import React, { useState } from 'react'
import Head from 'next/head'
import Link from 'next/link'

export default function Navbar({ children, home }) {
  const [toggleMobile, setToggleMobile] = useState(true)

  return (
    <nav className="nav__site">
      <div className="nav-desktop">
        <div className="nav-inner">
          <div className="nav-mobile-menu">
            <button
              onClick={() => setToggleMobile(!toggleMobile)}
              type="button"
              className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
              aria-controls="mobile-menu"
              aria-expanded="false"
            >
              <span className="sr-only">Open main menu</span>
              <svg
                className="block h-6 w-6"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                aria-hidden="true"
              >
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16" />
              </svg>
              <svg
                className="hidden h-6 w-6"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                aria-hidden="true"
              >
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12" />
              </svg>
            </button>
          </div>
          <div className="nav-brand">
            <div className="flex-shrink-0 flex items-center">
              <Link href="/">
                <a className="brand-title">EveryoneCanContribute</a>
              </Link>
              <span className="brand-next">Next</span>
            </div>
          </div>
          <div className="nav-link-container">
            <div className="hidden sm:block sm:ml-6">
              <div className="nav-links">
                <a href="#" className="nav-link">
                  About
                </a>
                <Link href="/members">
                  <a className="nav-link">Members</a>
                </Link>
                <a href="#" className="nav-link">
                  Handbook
                </a>
                <IconLink href="#" icon="/podcast.svg"></IconLink>
                <IconLink href="#" icon="/twitter.svg"></IconLink>
                <IconLink href="#" icon="/youtube.svg"></IconLink>
                <IconLink href="/discord" icon="/discord.svg"></IconLink>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className={`sm:hidden ${toggleMobile && 'hidden'}`}>
        <div className="px-2 pt-2 pb-3 space-y-1">
          <a href="#" className="bg-gray-900 text-white block px-3 py-2 rounded-md text-base font-medium">
            Dashboard
          </a>
          <a
            href="#"
            className="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium"
          >
            Team
          </a>
          <a
            href="#"
            className="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium"
          >
            Projects
          </a>
          <a
            href="#"
            className="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium"
          >
            Calendar
          </a>
        </div>
      </div>
    </nav>
  )
}

function IconLink({ href, icon }) {
  return (
    <Link href={href}>
      <a className="nav-icon">
        <img className="w-5" src={icon} />
      </a>
    </Link>
  )
}
