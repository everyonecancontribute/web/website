// Expand the alert with variants here
const variants = {
  info: 'bg-blue-200',
  success: 'bg-green-200',
}

export default function Alert({ children, className = '', variant = 'info', title }) {
  return (
    <div role="alert" className={`px-5 py-3 rounded inline-block ${variants[variant]} ${className}`}>
      {title && (
        <>
          <b className="mb-1 font-bold font-lg text-gray-800">{title}</b>:
        </>
      )}{' '}
      {children}
    </div>
  )
}
