import React, { useState } from 'react'
import Head from 'next/head'
import Date from './date'

export default function post({ children, html, date, title, authors = [] }) {
  const metaEntries = [authors && `by ${authors.join(', ')}`, <Date dateString={date} />].filter(Boolean)

  return (
    <>
      <Head>
        <title>{title}</title>
      </Head>
      <article className="post">
        <h1 className="text-2xl mb-2 font-bold">{title}</h1>
        <div className="post-meta">
          {metaEntries.map((meta, index) => (
            <span key={index}>{meta}</span>
          ))}
        </div>
        <div dangerouslySetInnerHTML={{ __html: html }} />
      </article>
    </>
  )
}
