---
title: "16. Cafe: Kubernetes deployments to Hetzner Cloud, step 3: k3s load balancer & pod resources"
date: "2021-02-10"
Aliases: []
Tags: ["gitlab","hetzner","cloud","terraform","ansible","kubernetes"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

> [Max Rosin](https://twitter.com/ekeih) kindly prepared a series of workshops to learn how to deploy Kubernetes in [Hetzner Cloud](https://www.hetzner.com/cloud). Hetzner generously sponsored cloud minutes for our sessions, thank you!

This session covers the third step, after having provisioned the server and agent VMs with Terraform and Ansible [in the first session](/post/2021-01-27-cafe-14-kubernetes-deployments-to-hetzner-cloud/) and deployed [k3s last week](/post/2021-02-03-cafe-15-kubernetes-deployments-to-hetzner-cloud-part-2/):

- Fix the cluster IPs with deploying the config with Ansible
- [Inspect pod resources on error](https://twitter.com/dnsmichi/status/1359554941665353731)
- Learn about [taint and tolerate error patterns](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/)
- Use Hetzner resources to manage the [load balancer](https://github.com/hetznercloud/hcloud-cloud-controller-manager)
- Encrypted secrets: [at rest](https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/) or [HashiCorp Vault](https://www.vaultproject.io/)
- Pods as group of containers, in a [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) or [ReplicaSet](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/) (which maintains a stable set of running pods)
  - Deployments are the recommended way to manage Pods as they are a higher-level concept that manages ReplicaSets and provides declarative updates to Pods along with a lot of other useful features like rollout history and rollbacks. More details in [this blog post on Kubermatic](https://www.kubermatic.com/blog/introduction-to-replicasets-deployment/).

Next week, we'll look into: 

- Ingress controller, to use a single load balancer for multiple websites to save costs. 
- Hetzner [storage volumes](https://github.com/hetznercloud/csi-driver)

Future ideas touch monitoring with Prometheus, GitLab CI/CD deployments and much more :) 

### Insights

- [Max's demo repository](https://gitlab.com/ekeih/k3s-demo) 
- [Twitter thread](https://twitter.com/dnsmichi/status/1359549041915396097)
- [12 Factor app](https://12factor.net/)
- [k3s Architecture](https://rancher.com/docs/k3s/latest/en/architecture/)


### Recording

Enjoy the session! 🦊 

{{< youtube fHyRrV0eUaU >}}



