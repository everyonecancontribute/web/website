---
title: "15. Cafe: Kubernetes deployments to Hetzner Cloud, step 2: k3s with Ansible"
date: "2021-02-03"
Aliases: []
Tags: ["gitlab","hetzner","cloud","terraform","ansible","kubernetes"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

> [Max Rosin](https://twitter.com/ekeih) kindly prepared a series of workshops to learn how to deploy Kubernetes in [Hetzner Cloud](https://www.hetzner.com/cloud). Hetzner generously sponsored cloud minutes for our sessions, thank you!

This session covers the second step, after having provisioned the server and agent VMs with Terraform and Ansible [last week](/post/2021-01-27-cafe-14-kubernetes-deployments-to-hetzner-cloud/):

- [Install wireguard](https://rancher.com/docs/k3s/latest/en/installation/network-options/) to use with Flannel and encrypt the network traffic
- Install the k3s binary and deploy a systemd unit using Ansible Jinja templates
- k3s agent setups requires an auth token on the master. [Use slurp and async task handlers](https://twitter.com/dnsmichi/status/1357027459154214912?s=20) to ensure the token is created on the master first, and then agents can consume it in their setup task.

Next week, we will dive into using the Hetzner resources to manage the [load balancer](https://github.com/hetznercloud/hcloud-cloud-controller-manager) and [storage volumes](https://github.com/hetznercloud/csi-driver). Future ideas touch monitoring with Prometheus, CI/CD pipeline deployments and much more :) 

### Insights

- [Max's demo repository](https://gitlab.com/ekeih/k3s-demo) with the [wireguard and k3s commit](https://gitlab.com/ekeih/k3s-demo/-/commit/5ffa9b98f2aa7e2c65eb6c19eb4b541c06d75569)
- [Twitter thread](https://twitter.com/dnsmichi/status/1357015648333926400)
- [k3s Architecture](https://rancher.com/docs/k3s/latest/en/architecture/)
- [Go Excusegen](https://gitlab.com/gitlab-de/go-excusegen)

### Recording

Enjoy the session! 🦊 

{{< youtube oGI06xr2PYk >}}


