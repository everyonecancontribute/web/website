---
title: "9. Cafe: AWS re:invent try-out: EC2 macOS & Proton"
date: "2020-12-02"
Aliases: []
Tags: ["aws","reinvent","cloudnative","macos"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

AWS re:invent announced new products and we decided to try them out:

- [Use Amazon EC2 Mac Instances to Build & Test macOS, iOS, ipadOS, tvOS, and watchOS Apps](https://aws.amazon.com/blogs/aws/new-use-mac-instances-to-build-test-macos-ios-ipados-tvos-and-watchos-apps/)
- [AWS Proton: A microservices/container deployment service](https://aws.amazon.com/blogs/aws/preview-aws-proton-automated-management-for-container-and-serverless-deployments/)

### Recording

Enjoy the session! 🦊 

{{< youtube KjSGXArtMSY >}}
