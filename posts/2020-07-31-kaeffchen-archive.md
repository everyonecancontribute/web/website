---
title: "#everyonecancontribute Kaeffchen: Archive"
date: "2020-07-31"
Aliases: []
Tags: ["gitlab","devsecops","archive","german"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

Our website and blog came from the first iteration where we started with a Google doc, and then later moved to GitLab projects, issues and this website deployed with pages. This blog post summarizes everything which happened thus far and adds the archive for older Kaeffchens as well. The meeting notes are untouched and provide the raw impressions, typos included. 

Everything in this blog post covers our **German Kaeffchen**. We made the decision to chat in German/Austrian and document the notes in English. Future iterations will introduce an [English](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/3171) and maybe Spanish ☕ chat too. 

You can follow the [Kaeffchen YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Ko8J8V5V794CXZUZ-DLxccI) and dive deeper below. 

### Blog posts

- [11. Kaeffchen](/post/2020-07-29-kaeffchen-11/)
- [10. Kaeffchen](/post/2020-07-22-kaeffchen-10/)
- [9. Kaeffchen](/post/2020-07-15-kaeffchen-9/)
- [8. Kaeffchen](/post/2020-07-08-kaeffchen-8/)
- [7. Kaeffchen](/post/2020-07-01-kaeffchen-7/)
- [6. Kaeffchen](/post/2020-06-17-kaeffchen-6/)
- [Welcome](/post/2020-05-28-first-post/)

### 2020-06-10: 5. Kaeffchen

- Agenda: https://gitlab.com/everyonecancontribute/general/-/issues/10 
- Recording: https://youtu.be/PfNTvuatI_A

#### Livestream

Will be streamed on our YouTube channel! [Click Me!](https://youtu.be/seEUMPhfxRo)

#### Website

- [everyonecancontribute.com](https://www.everyonecancontribute.com/) is live ❤️
- [everyonecancontribute.dev](https://www.everyonecancontribute.dev/) is the original demo site with a Prometheus Node Exporter (port 9100)
- First blog posts online, thanks MichaelA :100: 
  - Learn more about Mermaid in Markdown
- Tasks: Orga, Pagination, Logos, more content
- Added to the GitLab handbook: https://about.gitlab.com/handbook/marketing/community-relations/technical-evangelism/#projects 

#### Community 

- Group communication
  - Retire Twitter, Telegram groups
  - Use Gitter: https://gitlab.com/everyonecancontribute/general/-/issues/13 & https://gitter.im/everyonecancontribute/community# 
  - Naming schema:
    - Follow GitLab's schema: `contributors-en` and `contributors-de` maybe?
- How to join workflow: https://gitlab.com/everyonecancontribute/general/-/issues/14
- Document our framework and process for other communities, languages, etc. 

#### Misc

- [FYI] EA Command And Conquer Remastered Source Code [Open Sourced on Github](https://github.com/electronicarts/CnC_Remastered_Collection)

#### CI/CD

- Philipp: Working with WIP Status
  - Discussion about use cases
  - Trigger merge-request pipeline after removing WIP Status without a commit
    - I could share a snippet how to disable merge request pipelines when WIP is set
  - MichaelF: Renaming WIP to Draft is discussed: https://gitlab.com/gitlab-org/gitlab/-/issues/32692
- MichaelA: Block merge request via specific label like the `WIP:` prefix in the MR title? see gitlab-org/gitlab#220822
- MichaelF: [Release Management Think Big #12](https://www.youtube.com/watch?v=m-i3JuGBhIg) with deployment environments, location maps, heat maps, etc. ideas.
  - [Show alerts in environments](https://gitlab.com/gitlab-org/gitlab/-/issues/214634#note_345670102)

#### DevSecOps

- Bernhard: How to get to the metrics?
  - A short story about Apache httpd / Docker / Metricbeat & ElasticSearch ...
- Nico: Virtual Docker Rosenheim Meetup
  - [Policy & Governance für Kubernetes](https://www.meetup.com/Rosenheim-Docker-Meetup/events/271159440/)

#### Ideas

- MichaelF: Community activities _(no action needed)_
  - Forum speedrun - together with GitLab team members. https://gitlab.com/gitlab-com/marketing/community-relations/general/-/issues/23 


### 2020-06-03: 4. Kaeffchen

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/7)
- [Recording]()

#### General

- Servus Bernhard!

#### Security 

- [Grafana CVE-2020-13379](https://grafana.com/blog/2020/06/03/grafana-6.7.4-and-7.0.2-released-with-important-security-fix/)
- Kubernetes CVEs
  - https://github.com/kubernetes/kubernetes/issues/89378
  - https://github.com/kubernetes/kubernetes/issues/89377
- Docker CVE https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-13401

#### Website

- EveryoneCanContribute: http://everyonecancontribute.gitlab.io/

#### CI/CD
- Michael Aigner:
  - Possibilities of [dynamic pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html)
  - CI Pipeline “on issue” or “on comment” possible like in GitHub actions?
    - [API Events](https://docs.gitlab.com/ee/api/events.html)
    - [Triage Bot](https://csoriano.pages.gitlab.gnome.org/csoriano-blog/post/2019-01-07-issue-handling-automation/)
    - [Triage Ops](https://about.gitlab.com/handbook/engineering/quality/triage-operations/)
    - Feature Request: [Automatic Triage and Labelling](https://gitlab.com/gitlab-org/gitlab/-/issues/30748)
  - Artifacts in latest ref, does it really work in 13.0? See [NOTE](https://docs.gitlab.com/ee/ci/yaml/README.html#artifactsexpire_in) at the bottom of the  artifacts:expire_in section
    - Missed 13.0 label - [Docs MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/33776)

#### DevSecOps

- Michael Aigner: possible discussion-points for Tracing:
  - Tracing on Enduser-Device (Desktop-App, Mobile-App, non-Browser)
  - Store data types other than the normal float, double, int’s like Images, files, etc.
  - Push vs Pull in Tracing
  - Ideas:
     - Fast logs ingestion: https://www.youtube.com/watch?v=xzu245IqDbA 
     - Logging dev strategy, plus Elasticsearch + Logstash/Beats.
     - IoT message format
- Bernhard Rausch: How to get to the metrics?
  - A short story about Apache httpd / Docker / Metricbeat & ElasticSearch ...
  - Next week


### 2020-05-27: 3. Kaeffchen

[3. Kaeffchen Blog](https://dnsmichi.at/2020/05/27/3-everyonecancontribute-kaeffchen/)

#### General

- Welcome Michael #2 and Marcel :) (they come later)
- Everyonecancontribute.com - Content, hosting, subdomains, Logo
  - maybe use the animated version with the Kaeffchen Slogan in Comic Sans
  - Michael: Create group orga - https://gitlab.com/everyonecancontribute 
- [GitLab Commit CfP](https://about.gitlab.com/events/commit/) ends June 5th. Happens virtually on August 26th. 
  - Best practices, integrations, GitOps (Ansible, Terraform, Cloud, etc.)
  - https://www.meetup.com/gitlab-virtual-meetups/events/270752269/
  - https://www.youtube.com/watch?v=MK4zhYEYemw 
  - [Workshop](https://twitter.com/john_cogs/status/1265639239359832064) 
- Future of Remote work - [Twitter thread](https://twitter.com/sytses/status/1264341436138270720) by Sid
- Nico: GitLab Hero/Community insights
  - GitLab Heroes Steering Committee Meeting uploaded on Unfiltered
  - GitLab Commit: [GitLab Hero Panel Session](https://docs.google.com/document/d/1TY1UjBzbaQdFnSnBaoNkp6g01DptKrXKB7HJcJyDjt4/edit#) (ping me if you like to contribute)
  - [GitLab Heroes AMA](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes/-/issues/85) (feedback welcome)
  - [GitLab Heroes Day](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes/-/issues/77) (feedback welcome)

#### GitLab Insights

- More 13.0 insights
  - [Release blog post](https://about.gitlab.com/releases/2020/05/22/gitlab-13-0-released/) 
  - Memory usage reduced with Puma
  - Terraform state backend - WDYT?
    - Next week, try it out together?
- [How we release software patches](https://about.gitlab.com/blog/2020/05/13/how-we-release-software-patches/) 
- [CI/CD container images and builds on ARM Cortex](https://dev.to/dnsmichi/gitlab-ci-cd-pipelines-and-container-registry-for-arm-cortex-firmware-builds-4n4n) - answering a question on the forums 
- [KDE has migrated to GitLab](https://twitter.com/planetkde/status/1264223822292512768)

#### DevSecOps
- Michael: Tracing & Metrics 
  - [Slides](https://docs.google.com/presentation/d/1MAVFeSsTNVWC9wPGOlg83wh8GFtR9hPbdVHuumtgOWA/edit?usp=sharing)
- Nico: Kubernetes Networking
  - [SIG Networking](https://twitter.com/nmeisenzahl/status/1264484613772247040)
- Mario: GitLab 12.10 [Container registry expiration policy: enabled by default for new projects](https://gitlab.com/gitlab-org/gitlab/-/issues/208268)
- Compiler bugs - [memcpy optimization](https://twitter.com/SciresM/status/1257874707816144896)
- [Cron.weekly #135](https://ma.ttias.be/cronweekly/issue-135/)
- [rockaut/Markus]: Multi-Node TimescaleDB now in Free License (was Enterprise Only) ([Link to Blog](https://blog.timescale.com/blog/multi-node-petabyte-scale-time-series-database-postgresql-free-tsdb/))
- Alhought under one of the Common OSS Licenses


### 2020-05-20: 2. Kaeffchen

[2. Kaeffchen Blog](https://dnsmichi.at/2020/05/20/2-everyonecancontribute-kaeffchen/)

### General

- Michael: Hello Markus and Mario! Please introduce yourselves
- Michael: Streaming and Hosting
   - GitLab Unfiltered -> Stream target for live streams, currently disabled
   - Setup? Zoom? Streamyard? Stageenttv?
   - Michael: Zoom+YT = super easy
   - Michael: Streamyard has a 6 people limit, Stageenttv is more for single guests or a moderated show.
   - Markus: A livestreams is a good idea since people can peek in and maybe join the next time.
   - Michael: Zoom has a bug with YT streaming which is why this now disabled in the business client temporarily.
   - Jitsi (https://meet.jit.si/) tested and would work (Mario) 🆗
   - Zoom/OBS/YouTube tested and would work (Mario) 🆗
- Michael: Keeping the tech uptodate - 2nd iteration:
   - Update the weekly agenda on findings to talk about, including categories
   - #everonecancontribute “newsletter” here - WDYT?
- Mario: Addendum GitLab Runners in GCP cloud self-made (no Docker Machine)
  - Why? (maybe outdated)
  - How? (Preemtible, GitLab keepalived)
  - Benefits? (For us...) and open discussion (take away, ideas, …)
  - [Troubleshooting](https://docs.gitlab.com/runner/faq/)
- Michael: Hello Philipp! 

#### GitLab Insights

- Michael: GitLab 13.0 on Friday, May 22nd
  - [My favourites](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/2725) (shared on social media later) 
  - Dark theme for the Web IDE
  - Improved Merge Request Review views, better performance
- Michael: [Uncovering GitLab's tech stack and explore contribution possibilities](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/2736)

#### DevSecOps

- Michael: [DevSecOps report 2020](https://twitter.com/dnsmichi/status/1262386081812623366)
- Michael: [Zabbix 5.0 LTS](https://www.zabbix.com/whats_new_5_0)
- Michael: [Grafana 7.0](https://grafana.com/blog/2020/05/18/grafana-v7.0-released-new-plugin-architecture-visualizations-transformations-native-trace-support-and-more/)
- Markus: What you all think of [OpenMetrics](https://openmetrics.io/)/OpenTracing?
- Michael: Takes a bit longer, I’ll prepare something for next week
- Michael: [NXNS Amplification Attack](https://twitter.com/dnsmichi/status/1262869083462172672)



### 2020-05-13: 1. Kaeffchen

[1. Kaeffchen Blog](https://dnsmichi.at/2020/05/13/1-everyone-can-contribute-kaffchen/)

- Michael: Latest from GitLab (22nd release 13.0)
  - Monitoring suite moving to Core, minimal - watch out for alerts, incidents, etc.
  - Python package registry, later moved to Core
  - Peek into Release Management
  - CI/CD Parent-Child-Pipelines
  - 12.10: https://about.gitlab.com/releases/2020/04/22/gitlab-12-10-released/ 
    - only/except
  - Blog post: Matrix Builds for review: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/45795 
- Michael: GitLab Commit
  - Submit your story! 
- Michael: Learnings from GitHub Satellite
- Michael: Monitoring, Observability, Incidement Management
- Michael: Ideas
  - Translate typical English DevSecOps and CI/CD sayings into German
  - As a (community challenge) 
- Michael: Where are you looking at to keep you updated in tech?
  - Cron.weekly, Twitter, etc.
  - Michael: https://twitter.com/dnsmichi/lists
  - Nico: Google  
  - Moritz: Medium
- Nico: Runner mit Docker-Maschine noch ein Thema?
  - https://forum.gitlab.com/t/bug-idle-runners-are-still-active-after-the-idletimeout-time/37436/4 
- Nico: K8s Development
  - https://gitlab.com/groups/gitlab-org/-/epics/3230#note_340476660
