---
title: "10. Cafe: Raycast, level up your productivity"
date: "2020-12-09"
Aliases: []
Tags: ["productivity","workflows","macos"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

Michael Aigner prepared a greated live demo into [Raycast](https://raycast.com/). He also contributed [GitLab script commands](https://github.com/raycast/script-commands/pull/149) for our daily workflows.

### Recording

Enjoy the session! 🦊 

{{< youtube pBjZP1uhRUE >}}

