---
title: "12. Cafe: GitLab CI/CD & C++ ABI checks"
date: "2021-01-13"
Aliases: []
Tags: ["gitlab","cicd","c++"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

We started from [this tweet](https://twitter.com/axccl/status/1347665411722989568) asking about C++ ABI compatibility support in GitLab MRs. The discussion included some theoretical algorithm with a caching state machine inside the CI/CD pipeline. Michael Aigner prepared the [C++ project](https://gitlab.com/tonka3000/conan-cpp-example) and we implemented the solution together in the session.

Dynamic CI/CD pipeline generation was also touched, and the [for loop to generate](https://gitlab.com/instantlinux/docker-tools/-/blob/master/.gitlab-ci.yml#L38) parent/child pipelines in GitLab CI/CD.

### Recording

Enjoy the session! 🦊 

{{< youtube m26PB89UFtM >}}

