---
title: "2. Cafe: Vault: Basics & GitLab CI Secrets integration workshop"
date: "2020-09-30"
Aliases: []
Tags: ["vault","security","cicd","gitlab"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### General

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/39)
- Guests: [Niclas Mietz](https://twitter.com/solidnerd), [Michael Friedrich](https://twitter.com/dnsmichi), [Michael Aigner](https://twitter.com/solidnerd), [David Schmitt](https://twitter.com/dev_el_ops) 
- [Next ☕ chat `#3`](https://gitlab.com/everyonecancontribute/general/-/issues/46)

### Highlights

- [Vault](https://www.vaultproject.io/)
- [GitLab CI Secrets integration](https://docs.gitlab.com/ee/ci/secrets/)

### Recording

Enjoy the session!

{{< youtube ublw0_1FaI4 >}}

### Insights

- [Demo repository](https://gitlab.com/solidnerd/kaeffchen-vault-instance)
- [Hashicorp Vault Online Learning](https://learn.hashicorp.com/vault)
- [Vault VirtualBox Image](https://app.vagrantup.com/solidnerd/boxes/vault)
- [GitLab Direction: Secrets Management](https://about.gitlab.com/direction/release/secrets_management/)
