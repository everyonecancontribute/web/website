---
title: "29. Kaeffchen: Newsletters, HashiCorp CDK for Terraform, Service Meshes"
date: "2021-01-20"
Aliases: []
Tags: ["newsletter","terraform","cdk"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

We've looked into [Hashicorp's CDK for Terraform](https://www.hashicorp.com/blog/announcing-cdk-for-terraform-0-1) announcement, [newsletters to follow](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/social-media/#content-sources) and service meshes.

### Recording

Enjoy the session!

{{< youtube kRDpcIh3Vpc >}}
