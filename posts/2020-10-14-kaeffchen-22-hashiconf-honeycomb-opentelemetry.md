---
title: "22. Kaeffchen: HashiConf Boundary, Honeycomb, OpenTelemetry & JaegerTracing"
date: "2020-10-14"
Aliases: []
Tags: ["hashiconf","honeycomb","tracing","opentelemetry"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---


### Highlights

We started with a wild speculation on the new Hashicorp product to be announced at [HashiConf](https://hashiconf.com/digital-october/). CI/CD pipelines, monitoring or something around Vault and security and compliance management? Spoiler: In the evening, we learned about Boundary being the latter.

The discussion switched into metrics and tracing where David shared a Puppet blog post on [collecting GCP info and system metrics for Honeycomb](https://puppetlabs.github.io/iac/development/gcp/2020/10/05/honeycomb-gcp-metrics.html). He was so kind to live demo use cases for Honeycomb after reading more on OpenTelemetry.

We've also touched base with OpenTelemetry pipelines, inputs and exporters, whilst comparing it with JaegerTracing. 

### Recording

Enjoy the session!

{{< youtube xx4xJ7i7G2I >}}

### Bookmarks

- [Metrics, Tracing and Logging](https://peter.bourgon.org/blog/2017/02/21/metrics-tracing-and-logging.html)
- [OpenTelemetry Exporters](https://github.com/open-telemetry/opentelemetry-collector/tree/master/exporter)
- [Honeycomb.io](https://www.honeycomb.io/)
- [Collecting GCP info and system metrics for Honeycomb](https://puppetlabs.github.io/iac/development/gcp/2020/10/05/honeycomb-gcp-metrics.html)
- [JaegerTracing](https://www.jaegertracing.io/docs/1.20/getting-started/)
- [Jaeger Tracing in C++](https://github.com/Icinga/icinga2/pull/7745/commits/d7c0b725edc2fe614adb58de54b2959514d60d86) as profiling 
- [Observability Engineering](https://www.oreilly.com/library/view/observability-engineering/9781492076438/)
- [Tracing our Pipelines](https://testerbychoice.wordpress.com/2019/08/17/tracing-our-pipelines/)

### Insights

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/49)
- Guests: [Niclas Mietz](https://twitter.com/solidnerd), [Michael Friedrich](https://twitter.com/dnsmichi), [David Schmitt](https://twitter.com/dev_el_ops)
- Next ☕ chat `#23`: **2020-10-21** - [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/53) 


