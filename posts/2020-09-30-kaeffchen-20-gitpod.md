---
title: "20. Kaeffchen: GitLab with Gitpod"
date: "2020-09-30"
Aliases: []
Tags: ["ide","gitpod","learning"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### General

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/40)
- Guests: [Niclas Mietz](https://twitter.com/solidnerd), [Michael Friedrich](https://twitter.com/dnsmichi), [Michael Aigner](https://twitter.com/solidnerd), [David Schmitt](https://twitter.com/dev_el_ops) 
- Next ☕ chat `#21`: **2020-10-08** - [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/45) 

### Highlights

We explored the new GitPod environment [embedded into the GitLab repository view](https://www.gitpod.io/docs/gitlab-integration/) next to the Web IDE. 

![Gitpod Integration in GitLab](/post/images/gitpod_gitlab_integration.png)

Gitpod runs as a container workspace where you can:

- Access [Theia](https://www.gitpod.io/docs/ide/) which is Gitpod's IDE. VS Code uses the same. 
- Install extensions from the [VS Code marketplace](https://www.gitpod.io/docs/vscode-extensions/)
- Use `cmd+shift+p` to acces the configuration prompt 
- Access the shell to run CLI commands or inspect the environment. 
- Use Chrome's `More Tools > Create Shortcut` and tick `Open as window`. This creates a new Chrome app on macOS.

![Gitpod IDE](/post/images/gitpod_gitlab_view.png)

We've installed the [GitLab Workflow](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow). This needs a personal access token, which can be added with accessing the prompt (`cmd+shift+p`) and typing `gitlab setup`. This adds a menu item on the left side, and shows the CI status at the bottom. You can also inspect issues, MRs, etc. 

![Gitpod IDE GitLab Extension](/post/images/gitpod_gitlab_extension.png)

Later on, David suggested to try Puppet's PDK. We then installed the [Puppet extension](https://marketplace.visualstudio.com/items?itemName=jpogran.puppet-vscode) and figured that the PDK need to be installed into the workspace. At this point, we learned that Gitpod runs as container in a Kubernetes cluster and there's no sudo here.

The documentation unveils that you can build your own workspace with providing a custom Docker image which gets defined in the same Git repository. Challenge accepted! The first attempt included using the default Gitpod Docker image for Ubuntu, and [installing the PDK](https://puppet.com/try-puppet/puppet-development-kit/).

Gitpod stores all settings in a file called [.gitpod.yml](https://www.gitpod.io/docs/config-gitpod-file/). Since the workspace is using the GitLab repository as clone, you can navigate in the IDE to [git add, commit and push](https://www.gitpod.io/docs/git/). Or you use the terminal CLI commands, or vim even. 

```
.gitpod.yml

image:
  file: Dockerfile.gitpod
```

The [Dockerfile](https://www.gitpod.io/docs/config-docker/#using-a-dockerfile) may need to override `USER`, `WORKDIR` and `ENTRYPOINT`.

```
Dockerfile.gitpod

FROM puppet/pdk

USER root
WORKDIR /

ENTRYPOINT []
```

You can follow the progress and final result in [this MR](https://gitlab.com/dnsmichi/puppet-gitlab/-/merge_requests/1/diffs). We ended up with some path problems when creating a new Puppet class, a thing for another session.

Next week, we'll explore more programming languages together with Gitpod. Rust is the top vote currently :)

### Recording

Enjoy the session!

{{< youtube 6c0Vfn8BIM4 >}}

### Bookmarks

- GitLab and GitPod: [Intro video by Marcel van Remmereden](https://www.loom.com/share/9c9711d4876a40869b9294eecb24c54d)
- [Feature request: draw.io integration into GitLab wiki](https://gitlab.com/gitlab-org/gitlab/-/issues/20305)
- [Git CLI merge_options](https://docs.google.com/presentation/d/11U_ZIuqoUfPUV5ctc9K6QCHvID7xSdpJpoU6ZgwdnqQ/edit#slide=id.g93655b10a9_0_446)
- [Wireflow](https://github.com/vanila-io/wireflow), a new OSS flow chart app written in NodeJS
- [Stacked Pull Requests for faster code reviews](https://www.michaelagreiler.com/stacked-pull-requests/)
- [Instrumenting using @honeycombio buildevents to discover optimizations for @bazelbuild](https://twitter.com/rvine_naidu/status/1308716324743307266?s=20)
- [Lovable code reviews?](https://twitter.com/dnsmichi/status/1310536231558807552)
- [How do you install Ansible?](https://twitter.com/realrockaut/status/1310534984755810306?s=27)
- [Falco uses CII best practices (C++)](https://twitter.com/leodido/status/1310642607064788992?s=27)
- [Python behind the scenes #2: how the CPython compiler works](https://tenthousandmeters.com/blog/python-behind-the-scenes-2-how-the-cpython-compiler-works/)





