---
title: "17. Kaeffchen: Podcast statistics and Docker Hub rate-limiting"
date: "2020-09-09"
Aliases: []
Tags: ["docker","podcast","statistics","dns"]
Categories: ["Community"]
Authors: ["m4r10k"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### General

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/36)
- Guests: Bernhard Rausch, Niclas Mietz, Michael Aigner, Mario Kleinsasser 
- Next ☕ chat `#17`: **2020-09-16** - [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/37) 

### Highlights

- Discussion about the Docker Hub rate limiting
  - [Announcement](https://www.docker.com/blog/scaling-docker-to-serve-millions-more-developers-network-egress/)
  - [Docker registry mirror manifest pull](https://docs.docker.com/registry/recipes/mirror/)
  - [Check if new DockerHub pricing will affect CI Runners](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/11113)
  - [Dragonfly Registry](https://d7y.io/en-us/)
  - [Perkeep](https://perkeep.org/)
  - What will be the future?

- Some updates about our podcast from the Anchor.fm stats
  - https://anchor.fm can be used by everyone for free to distribute a podcast

### Recording

Enjoy the session!

{{< youtube YqUOqTg4xxk >}}

### Insights

The discussion was very interesting because we discussed a lot of different ideas how to solve the Docker container registry problem in the future. There where some nice aspects which already existing system can be used to resolve the fragmentation of the Docker Hub registries.



