---
title: "11. Cafe: 5 min prod app with Vue & GitLab CI template insights"
date: "2020-12-16"
Aliases: []
Tags: ["cicd","deployment","templates","gitlab"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

Michael provided development insights into the [5 min production app](https://about.gitlab.com/blog/2020/12/15/first-code-to-ci-cd-deployments-in-5-minutes/) and the [live demo with VueJS](https://gitlab.com/gitlab-de/5-min-prod-app-vuejs) he was working on at AWS re:invent ([Tweet thread](https://twitter.com/dnsmichi/status/1339201532416372737)).

We've also discussed CI/CD templates with GitLab and found valuable resources:

- [Includes](https://docs.gitlab.com/ee/ci/yaml/includes.html)
- [Built-in templates](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates)
- [Blog: 3 YAML tips for better pipelines](https://about.gitlab.com/blog/2020/10/01/three-yaml-tips-better-pipelines/)
- [Security Webcast CI configuration](https://gitlab.com/dnsmichi/ci-security-webcast-2020/-/blob/master/.gitlab-ci.yml)
- [Test includes with the same job name](https://gitlab.com/dnsmichi/ci-cd-playground/-/merge_requests/6/diffs)
- [Anchors and extends example](https://git.netways.de/packaging/pipelines/-/blob/master/rpm-base.yml)

### Recording

Enjoy the session! 🦊 

{{< youtube SI_G6k_9Cds >}}

