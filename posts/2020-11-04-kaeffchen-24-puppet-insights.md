---
title: "24. Kaeffchen: Puppet Insights - 7 preview, Learn, Remediate & Comply, Bolt, Puppetize Digital"
date: "2020-11-04"
Aliases: []
Tags: ["puppet","insights","security"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---


### Highlights

- Puppet 7 Release within the next couple of weeks. Preview:
  - [Nightly Builds](https://nightlies.puppetlabs.com/)
  - Facter 4 written in Ruby again, no C++. Maintenance and contributions. 
  - Newer components: Ruby 2.7, postgresql 11+, default to SHA256
  - Environment caching improvements
- [Learn Puppet](https://learn.puppet.com/)
  - Trainings in Germany: [example42](https://example42.com/puppet/)
  - Self-paced hands-on training: [Learning VM](https://puppet.com/download-learning-vm) and [Learning VM video series](https://www.youtube.com/playlist?list=PLV86BgbREluVslX1cl2X1DQ8sc-uRwNad)
- [Pupperware](https://github.com/puppetlabs/pupperware): docker-compose for a puppetserver and database
- Puppet Security products
  - [Remediate](https://puppet.com/products/puppet-remediate/) for vulnerability scanning
    - use existing security scanners and integrations
    - Execute fixes with Bolt
  - [Comply](https://puppet.com/products/puppet-comply/) for compliance scanning
    - Integrate with CIS
    - manage profiles and baselines
- [Puppet Bolt](https://puppet.com/open-source/bolt/)
- [Puppet Litmus](https://puppetlabs.github.io/litmus/)
- [Puppetize Digital](https://digital.puppetize.com/s/landing-page4/home)

### Recording

Enjoy the session!

{{< youtube DMyjFoA6xCM >}}


### Insights

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/59)
- Guests: [Michael Friedrich](https://twitter.com/dnsmichi), [David Schmitt](https://twitter.com/dev_el_ops)
- Next ☕ chat `#25`: **2020-11-11** - [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/61) 











