---
title: "5. Cafe: HashiCorp Waypoint workshop"
date: "2020-10-21"
Aliases: []
Tags: ["hashicorp","waypoint","gitlab","cicd"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

We started with some insights from last Thursday and a general introduction to HashiCorp Waypoint by Brendan. The CI/CD configuration is something Abubakar is working on for bringing [Waypoint Docker images](https://gitlab.com/gitlab-org/waypoint-images) (pre-installed, useful entrypoints) into OOTB [CI/CD template includes for Auto-DevOps](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/45314/diffs#54bda35b7fd450b5d6b412a1614f35bfece1aabb).

We then went to changing Brendan's [demo](https://gitlab.com/brendan-demo/waypoint) into using the new image - live in the session. Last but not least, we discussed the roadmap and future thoughts around Waypoint.


### Recording

Enjoy the session! 🦊 

{{< youtube _OSDh_L5M_E >}}


#### Bookmarks 

- [Blog](https://about.gitlab.com/blog/2020/10/15/use-waypoint-to-deploy-with-gitlab-cicd/)
  - [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/65258)
- [Docs](https://www.waypointproject.io/docs/automating-execution/gitlab-cicd)
  - [Pull Request](https://github.com/hashicorp/waypoint/pull/492)
  - [Demo Repo](https://gitlab.com/brendan-demo/waypoint)
- Social
  - [Mitchell Hashimoto](https://twitter.com/mitchellh/status/1317225464297967616)

##### Resources

- https://www.hashicorp.com/blog/announcing-waypoint 
- https://github.com/hashicorp/waypoint
- https://www.waypointproject.io/docs/getting-started
- https://learn.hashicorp.com/waypoint 
- https://www.waypointproject.io/docs/roadmap
  - Cloud database access: https://www.waypointproject.io/docs/roadmap#service-brokering-databases-queues-etc 
- https://www.waypointproject.io/docs/internals/execution

### Insights

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/54)
- Guests: [Brendan O'Leary](https://twitter.com/olearycrew), [Abubakar Siddiq Ango](https://twitter.com/sarki247), [Niclas Mietz](https://twitter.com/solidnerd), [Michael Friedrich](https://twitter.com/dnsmichi), [Michael Aigner](https://twitter.com/solidnerd), [Marcel Weinberg](https://twitter.com/winem_), [David Schmitt](https://twitter.com/dev_el_ops)
- [Next ☕ chat `#6`](https://gitlab.com/everyonecancontribute/general/-/issues/55)

For async reading:

- [Service Mesh visualization in Consul 1.9](https://twitter.com/HashiCorp/status/1317210463843897344)
- [Understanding DNS](https://twitter.com/labeveryday/status/1313467434712563714?s=27)
- [CI/CD Pipeline Authoring 3y vision](https://twitter.com/dov0211/status/1317780039187005440?s=27)
- Brendan's Homelab
  - https://www.portainer.io/
  - https://heimdall.site/


