---
title: "4. Cafe: Jina.ai, an open source neural search framework in the cloud"
date: "2020-10-14"
Aliases: []
Tags: ["jina","neuralsearch","ai"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

Alex, Pei-Shan, Rutuja and Pratik joined for an introduction in [Jina](https://jina.ai/), an open source framework for neural search in the cloud. After a brieve introduction, we learned how to detect Pokemon images with then diving into the deep down technical designs. We've also learned about their roadmap and ways to start contributing. 

### Recording

Enjoy the session! 🦊 

{{< youtube Lchw4lQG8y0 >}}


#### Bookmarks 

- [Repository](https://github.com/jina-ai/jina)
- [Examples](https://github.com/jina-ai/examples)
- [Documentation](https://docs.jina.ai/index.html)
- [Contribute to Jina.ai](https://github.com/jina-ai/jina/blob/master/CONTRIBUTING.md)
- [Slack](https://docs.jina.ai/chapters/CONTRIBUTING.html#join-us-on-slack)


### Insights

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/48)
- Guests: [Alex Cureton-Griffiths](https://twitter.com/alexcg), [Pei-Shan Wu](https://twitter.com/pswu11), [Rutujua Surve](https://www.linkedin.com/in/rutuja-surve-150b4080/), [Pratik Bhavsar](https://www.linkedin.com/in/bhavsarpratik/), [Niclas Mietz](https://twitter.com/solidnerd), [Michael Friedrich](https://twitter.com/dnsmichi), [Michael Aigner](https://twitter.com/solidnerd)
- [Next ☕ chat `#5`](https://gitlab.com/everyonecancontribute/general/-/issues/54)
