---
title: "13. Kaeffchen: Events, community, social, education, how to google correctly, chaos engineering"
date: "2020-08-12"
Aliases: []
Tags: ["gitlab","culture","events","education","google"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### General

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/27)
- Guests: Niclas Mietz, Carsten Köbke, Michael Friedrich, Michael Aigner, Nico Meisenzahl, Mario Kleinsasser 
- Next ☕ chat `#14`: **2020-08-19** - [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/30) - KubeCon Kaeffchen


### Highlights

- KubeCon & Commit talks
- Socializing on events is missing
  - Hallway tracks and open spaces to meet the community
- Education tracks
  - Teach others how to get going
- Use case stories which solved real problems
  - Learn the starting point of a problem solution
- The "Why" Story
  - Add this to blog posts, docs, etc.
- How to educate people 
  - Visit [Feu's GitLab Commit talk](https://gitlabcommitvirtual2020.sched.com/event/dUWM/teaching-the-basics-about-gitlab)!
- How to Google correctly?
  - Keywords, experience, tactics and share this with everyone
- How to analyse an error message 
- Error culture with Chaos Engineering

Idea for next week:

- Pick an issue/forum topic and google together working on a solution
  - Solve a problem with different languages or containers

### Recording

Enjoy the session!

{{< youtube Nub4J2UDOz0 >}}

### Insights

- [GitLab Commit Schedule](https://gitlabcommitvirtual2020.sched.com/?timezone=Europe%2FBerlin)
- [KubeCon Schedule](https://kccnceu20.sched.com/) 
- KubeCon & Commit talks
  - Community focus in tracks?
  - Too big?
  - Like FOSDEM and Froscon 
- Socializing on events is missing
  - How will the coming months go?
  - Smaller camps, meetups or still topic focussed conferences.
- Education tracks 
  - Coding labs for kids
  - Move young generation towards technology
  - How did you solve this? Share with others
  - [GitLab WG](https://about.gitlab.com/company/team/structure/working-groups/upstream-diversity/)
- Use case stories which solved real problems
  - Science, not too much technology
  - Follow the thoughts 
  - **Get the starting point**
- The "Why" Story
  - [Start with why -- how great leaders inspire action | Simon Sinek | TEDxPugetSound](https://www.youtube.com/watch?v=u4ZoJKF_VuA)
  - Context is missing, which thoughts lead to the solution?
- How to educate people 
  - Getting Started with CI/CD - exit 1 - fail, exit 0 - 💡
  - Feu's How to teach Git/GitLab at Commit
  - Break down in smaller pieces - Kubernetes from the beginning
  - Braindump blog posts - do it more 
  - [Git for ages 4 and up](https://www.youtube.com/watch?v=3m7BgIvC-uQ) 
- How to Google correctly?
  - We know how to combine words for the best results
  - Google correctly: Remove results with `-`
  - Nobody said the first search must be successful 
  - Incognito tabs for searching
  - Open URL - watch the date, threshold 
- How to analyse an error message
  - Copy paste to Google
  - Haystack the stack trace 
  - Wall of text   
- Live challenge
  - Search for something
  - Disaster topic 
- Error culture
  - Test drive a new topic until it is "dead" or "breaks", then you are good to go
  - "Prepare for change, not for plan"
  - GitLab public problems - visible in GDoc and GitLab.com is down. 
  - Chaos Engineering - AWS mit chaos region 

Idea for next week:

- Pick an issue/topic and try to research and solve it challenge for next time
  - Solve a problem with different languages or containers

### News

- [Massive 20GB Intel IP Data Breach Floods the Internet, Mentions Backdoors](https://www.tomshardware.com/news/massive-20gb-intel-data-breach-floods-the-internet-mentions-backdoors)
- [Microsoft Patchday](https://twitter.com/dnsmichi/status/1293507240222236674)
  - `A spoofing vulnerability exists when Windows incorrectly validates file signatures. An attacker who successfully exploited this vulnerability could bypass security features and load improperly signed files.`
- [MatLab and GitLab CI](https://forum.gitlab.com/t/did-somebody-bring-matlab-project-with-gitlab-runner-to-work/40325/3)
- [Git graph in Code](https://twitter.com/tonka_2000/status/1291341948654096385?s=12)
- [Building dashboards for operational visibility](https://aws.amazon.com/builders-library/building-dashboards-for-operational-visibility/)
  - [Make Runtime Data useful in monitoring](https://twitter.com/flolaut/status/1292022865865453570?s=12)
  - The most useful part of this article is the `Dashboard maintenance` section, at the bottom, the smallest one :smirk: (m4r10k)
- [What is a Product Roadmap?](https://twitter.com/olearycrew/status/1292823174845997056)
- [Terraform Pipelines in GitLab](https://timberry.dev/posts/terraform-pipelines-in-gitlab/)
- [Speeding up GitLab CI Laravel Tests with a Custom Docker Image](https://www.phpjobs.app/blog/speeding-up-gitlab-ci-laravel-tests-with-a-custom-docker-image)
- [Twitter Discussion on Merge Request Workflows](https://twitter.com/dnsmichi/status/1293216297464016897)

### Bookmarks

- [Best Commands for learning Git](https://twitter.com/lisabyrnedev/status/1291795323627507712)
- [`git rebase --onto`](https://twitter.com/mluisbrown/status/1291756770445099009?s=12)
- [Show Code Coverage on GitLab CI](https://www.cypress.io/blog/2019/10/22/show-code-coverage-on-gitlab-ci/)



