---
title: "Boost your productivity with raycast"
date: "2020-12-10"
Aliases: []
Tags: ["raycast", "macos", "productivity"]
Categories: ["Community"]
Authors: ["tonka3000"]
---

![raycast](../images/raycast-fullscreen.png)

[Raycast](https://raycast.com) is a productivity tool. It works very similar as Spotlight on MacOS, but on steroids.

> For a live demo, checkout the [10. Cafe recording](/post/2020-12-09-cafe-10-raycast/).

Here is a short quote from the [Raycast presskit](https://www.notion.so/Raycast-Press-Kit-ce1ccf8306b14ac8b8d47b3276bf34e0) about their vision:

> Raycast is an app that provides developers with quick access to their tools. The app reduces the continuous context switching that millions of developers face every day with a command line inspired interface. It connects with Jira, GitHub and G Suite among others. The vision is to create a platform and developer community around productivity tools. An API allows developers to build custom integrations to speed up common tasks and share them with their team or the community.

The tool is pretty new on the market. It was built in the last past year in private by Thomas Paul Mann (CEO) and Petr Nikolaev (CTO).

![raycast-founders](../images/raycast_founders.jpg)

# How Raycast works

As already mentioned rRycast is a spotlight-like app specifically built for developers. The default hotkey for Raycast is `alt` + `space` but you can change this setting at any time.

![raycast-hotkey](../images/raycast_hotkey.png)

When you press the hotkey you get a spotlight like input bar, but you will also see suggestions for commands and all commands which are available. Like spotlight, you can start typing and the list will show the search results. It is a fuzzy search, so you can type only parts of the command and Raycast will find it.

![raycast-inputbar](../images/raycast_inputbar.png)

Right now it looks like classic spotlight, so what is the difference - the extensions. The extension system is powerful and there are cool extensions like GitHub or a hacker news reader directly integrated into raycast - goodbye context-switch 👋.

![raycast-hackernews](../images/raycast_hackernews.png)

It supports lists and forms, but you can also have actions like `open in browser` or `Copy to clipboard` based on the selected line via `CMD` + `K` in the list view like in the Jira extension.

![raycast-jira](../images/raycast_jira.png)

# Builtin extensions

There are a lot of builtin extensions like

- GitHub
- Jira
- G Suite
- Zoom
- Hacker news reader

and many more. Just checkout the `Extensions` tab in the preferences.

You can easily activate extensions by clicking on the extension in the `Extensions` tab. For example, here's the GitHub extension:

![github extension settings](../images/raycast_github_ext_settings.png)

Just press the `Log In` button and login with typical OAuth flow. After this step you can use the GitHub commands in Raycast.

## Alias commands

You can also use aliases for daily tasks so that you have to type less 🎉. If you don't want to type anything you can also use global hotkeys as well 🎉🎉🎉.

![raycast-shortcuts](../images/raycast_shortcuts.png)

## Script commands

There is no official SDK/API to write your own extensions today, but this will change in 2021. Until then, you can still write your own scripts to make your life easier. Writing script commands is not that hard and you can write it in a variety of languages: bash, python, nodejs, swift and more. There is an official [scripts repo](https://github.com/raycast/script-commands) which you can download/clone to your machine and use them or create your own ones.

### Extension vs Script command

A script command is just a "simple" script. You can not use the list views or the forms from the script command. You can print to stdout from your script and display it in Raycast like any CLI tool print the stdout into the terminal.

### How to activate a script command in Raycast

You need to activate your script to be able to use it in Raycast. Raycast does not care where the scripts are stored. You have to tell Raycast where to find them. Navigate into the `Preferences` -> `Extensions` tab and click on `Script Commands` on the left sidebar. Next click on `Add More Directories` and add the directory which contains your script (Raycast does not perform a recursive search in sub directories by design). It is also good to activate `Auto Reload` during development or experimentation so that Raycast will reload the command on every file change otherwise you have to click on reload yourself anytime you change the script.

![raycast-script-directories](../images/raycast_script_directories.png)

You can use the script from the [Raycast script-commands repository](https://github.com/raycast/script-commands) but be aware that you have to manually copy the scripts to your script directory (which you added to Raycast before) to be able to use them. Also some scripts require manual change like setting an API token in the script itself. So make sure to read the comments in the script you want to use!

The benefit of this design is that you can easily share a repo for your common script task with e.g. your whole team.

### How to build your own script command

You need at least just one file. As an example I use my already merge [Open Gmail in the browser](https://github.com/raycast/script-commands/pull/151) script which is just a simple shell script as shown below which just open gmail.com in your default browser
<script src="http://gist-it.appspot.com/https://github.com/raycast/script-commands/blob/master/commands/communication/gmail/google-gmail.sh"></script>

Make sure that the file has an execution flag (`chmod +x`). The comments with `@raycast` are instructions for Raycast. Most of them self-explanatory.
- title
  Title of the command which you can search for
- packageName
  The name of the package which is shown as subtitle on the right side of the command
- icon
  The relative path to your icon
- mode
  The mode is really important. I choose `silent` in the Gmail case because i don't want to show any output in Raycast itself.
  You can also choose
  - fullOutput
    Command prints entire output on separate view.
  - compact
    Command shows a toast while running in background.
  - inline
    Sets the script up to be shown as refreshing dashboard command where the output is displayed inline in the item (make sure to also specify a refreshTime)

If you want to know all details just checkout the table in the [README](https://github.com/raycast/script-commands#api).

Below the Raycast comments are just normal bash commands, so you can use anything you like here. Also this script can have any type of complexity.

You need to activate your script to be able to test it in Raycast. This just works the same as described above in the `How to activate a script command in Raycast` section. You can also to activate `Auto Reload` during development so that Raycast will reload the command on every file change otherwise you have to click on reload yourself anytime you change the script.

If you need arguments for your script you can add up to 3 arguments like in the amazon example from below.

<video src="https://raycast.com/uploads/script-single-argument-amazon.mp4" style="width: 100%" autoplay loop></video>

### References

I already built 2 script commands and you can use them as reference if you want
- ["open gmail.com" in your default browser](https://github.com/raycast/script-commands/pull/151)
  which is really simple one and in bash
  ![gmail in the browser](https://user-images.githubusercontent.com/3163807/101375568-d641c980-38af-11eb-91b6-6d0c57829ff1.png)
- [GitLab commands](https://github.com/raycast/script-commands/pull/149)
  for
  - ToDo List
  - Merge requests List
  - Issue List
  which show you the same content as the quick action in the upper right corner of the gitlab website itself.

  ![gitlab-commands](https://user-images.githubusercontent.com/3163807/101289164-0d09d800-37fb-11eb-9aa5-542db22c0a7c.gif)


## Conclusion

Raycast is a really young project but is pretty powerful right now. I like the product and the vision behind it and so I will continue to use it. The only real downside (for me personally) is that there is no Windows version (so that I can use it at work) and I think this will still be the case in 2021. Hopefully the API/SDK will be released soon so that I can make a full integration of GitLab like the builtin GitHub extension. This tool has a big potential to save developers a lot of time during the day.

<p style="font-weight: bold; font-size: 25px;">So if you want to boost your productivity just try out Raycast yourself!</p>



Info about images and video sources: Some images and videos are from their [Press Kit](https://www.notion.so/Raycast-Press-Kit-ce1ccf8306b14ac8b8d47b3276bf34e0) and from https://raycast.com