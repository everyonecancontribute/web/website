---
title: "27. Kaeffchen: 5 min production app & local Kubernetes development"
date: "2020-12-09"
Aliases: []
Tags: ["gitlab"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

We looked into the [5 min production app with VueJS](https://gitlab.com/gitlab-de/5-min-prod-app-vuejs) and [Google Skaffold](https://skaffold.dev/) for local Kubernetes development.

### Recording

Enjoy the session!

{{< youtube mxR7HxDrnrw >}}
