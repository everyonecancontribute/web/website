---
title: "26. Kaeffchen: macOS Big Sur & Tracing"
date: "2020-12-02"
Aliases: []
Tags: ["tracing","macos","limit"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

We've shared the latest news from the week, including the [macOS Big Sur update](https://dnsmichi.at/2020/11/30/upgrade-to-macos-big-sur/) with then moving to tracing, OpenTelemetry and Honeycomb's [buildevents](https://github.com/honeycombio/buildevents) and how CI/CD tracing in general can be improved.

### Recording

Enjoy the session!

{{< youtube 1K_sJ7OrwTY >}}
