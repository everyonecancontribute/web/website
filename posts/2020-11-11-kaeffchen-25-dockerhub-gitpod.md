---
title: "25. Kaeffchen: Docker Hub Rate Limit Monitoring & Gitpod"
date: "2020-11-11"
Aliases: []
Tags: ["dockerhub","gitpod","limit"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---


### Highlights

We've talked a bit about GitLab, then jumped into Docker Hub Rate Limit monitoring and concluded with Gitpod tests for about.gitlab.com. 

### Recording

Enjoy the session!

{{< youtube D10r6qtUiho >}}


### Insights

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/59)
- Guests: [Michael Friedrich](https://twitter.com/dnsmichi), [Christian Stankovic](https://twitter.com/stankowic_devel), [Nicolai Buchwitz](https://twitter.com/NicolaiBuchwitz), [Niclas Mietz](https://twitter.com/solidnerd)
- Next ☕ chat `#26`: **2020-11-25** - [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/61) 












