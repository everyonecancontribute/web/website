---
title: "14. Cafe: Kubernetes deployments to Hetzner Cloud, step 1: Terraform & Ansible"
date: "2021-01-27"
Aliases: []
Tags: ["gitlab","hetzner","cloud","terraform","ansible","kubernetes"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

> [Max Rosin](https://twitter.com/ekeih) kindly prepared a series of workshops to learn how to deploy Kubernetes in [Hetzner Cloud](https://www.hetzner.com/cloud). Hetzner generously sponsored cloud minutes for our sessions, thank you!

This session covers the first steps with Terraform and Ansible, [next week](/post/2021-02-03-cafe-15-kubernetes-deployments-to-hetzner-cloud-part-2/), Kubernetes will be deployed on top.

- [Max's demo repository](https://gitlab.com/ekeih/k3s-demo) with [Terraform code](https://gitlab.com/ekeih/k3s-demo/-/commit/e1ada5d8f00f928c3f7655e7a82112b7e7df06fd) and [Ansible code](https://gitlab.com/ekeih/k3s-demo/-/commit/b7d318bedb4a1a64b59189c05dee265c1ac07660)
- [GitLab Direction for Infrastructure as Code](https://about.gitlab.com/direction/configure/infrastructure_as_code/)

GitLab features:
- [Protected Terraform states (developer access)](https://gitlab.com/gitlab-org/gitlab/-/issues/227108)
- [Terraform MR widget integration, next steps](https://gitlab.com/groups/gitlab-org/-/epics/3441)
- [5 minute production app with Terraform in the background](https://about.gitlab.com/blog/2020/12/15/first-code-to-ci-cd-deployments-in-5-minutes/)
- [Terraform Registry](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/18834)
- [Move Kubernetes cluster creation into Terraform](https://gitlab.com/gitlab-org/gitlab/-/issues/220201)

### Recording

Enjoy the session! 🦊 

{{< youtube LvFvQmqce5o >}}

