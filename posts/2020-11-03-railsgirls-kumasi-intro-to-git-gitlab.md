---
title: "RailsGirls Kumasi: Intro to Git & GitLab"
date: "2020-11-03"
Aliases: []
Tags: ["git","gitlab","learn"]
Categories: ["Community"]
Authors: ["abuango"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

Owing most of my career successes to the developer communty, I am alawys eager to give back. When the RailsGirls Kumasi (Ghana) reached out to GitLab for a speaker, I was eager to signup. Despite having challenges with Internet connectivity, which limited interactions with attendees, I was able to showcase Git and GitLab with a demo, while recording the session to make it available for the attendees to watch on-demand. You can also watch the recording below, the [slide deck used in the presentaion](https://gitlab-training.gitlab.io/basics/deck/both.html#/) is also publicly available.

### Recording

Enjoy the session! 🦊 

{{< youtube E5pe2jrOZpc >}}








