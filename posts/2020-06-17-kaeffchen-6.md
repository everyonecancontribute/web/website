---
title: "6. Kaeffchen Summary"
date: "2020-06-05"
Aliases: []
Tags: []
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### General

[Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/15)

**Important**: Our next Kaeffchen happens on Wed, July 1st, 16:00 CEST. Next week we are taking a break.

### Guests

Mario Kleinsasser, Michael Aigner, Philipp Westphalen, Nico Meisenzahl, Anton Dollmaier, Konstantin Wolff, Greg Campion, Michael Friedrich

### Highlights

Website updates, review app for changes, [Handbook first](https://www.youtube.com/watch?v=SVN8B8ugqNg) & collaboration and documentation ([Static Site editor](https://gitlab.com/groups/gitlab-org/-/epics/2688)), [Paessler webcast](https://learn.gitlab.com/c/paessler-goes-gitlab?x=F_lAHF), protecting CI configuration & DevSecOps culture. 

### Recording

Enjoy the session!

{{< youtube 2W4SssIID3g >}}
