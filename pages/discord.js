import Head from 'next/head'
import Layout from '../components/layout'
import { ChatAlt, LockClosed, CursorClick, Bell, Cube, UserGroup, LightningBolt } from 'heroicons-react'
import paths from '../data/discord-table'
import { siteTitle } from '../lib/constants'
import Button from '../components/button'
import Alert from '../components/alert'

export default function Members() {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle} - Discord</title>
      </Head>
      <section>
        <div className="flex items-center text-3xl font-bold mb-5">
          <ChatAlt size="30" />
          <span className="ml-2">Discord</span>
        </div>
      </section>
      <div>
        <Alert variant="info" title="Note">
          We have tried Twitter DM groups, Telegram groups and Gitter channels before. Discord is the next iteration. 👣
        </Alert>
        <p className="mt-8">
          <Button href="https://discord.gg/qgQWhD6wWV">Join Discord now</Button>
        </p>
        <div className="flex items-center text-2xl mb-5 mt-10 text-gray-600">
          <LockClosed size="20" />
          <span className="ml-2">Permissions</span>
        </div>
        <p>
          The <span className="font-bold text-purple-600">everyonecancontribute</span> server has the following roles:
        </p>
        <ul className="list-disc list-inside mt-3 ml-5">
          <li>
            <span className="font-bold text-purple-600">admin</span> for group founders
          </li>
          <li>
            <span className="font-bold text-purple-600">everyone</span> for everyone else
          </li>
        </ul>
        <div className="flex items-center text-2xl mb-5 mt-10 text-gray-600">
          <CursorClick size="20" />
          <span className="ml-2">Integrations</span>
        </div>
        <div className="flex items-center text-xl mb-5 mt-10 text-gray-600">
          <LightningBolt size="20" />
          <span className="ml-2">Bots</span>
        </div>
        <p>TBD</p>
        <div className="flex items-center text-xl mb-5 mt-10 text-gray-600">
          <Bell size="20" />
          <span className="ml-2">GitLab Notifications</span>
        </div>
        <p>
          The Discord server has configured webhooks for GitLab groups following the{' '}
          <a
            className="text-blue-700 hover:underline"
            href="https://docs.gitlab.com/ee/user/project/integrations/discord_notifications.html"
            target="_blank"
          >
            documentation
          </a>
          :
        </p>
        <table className="text-left mt-4">
          <thead>
            <tr>
              <TableHead>GitLab Path</TableHead>
              <TableHead>Channel</TableHead>
              <TableHead>Enabled</TableHead>
            </tr>
          </thead>
          <tbody>
            {paths.map((path) => {
              return (
                <tr>
                  <td className="py-4 px-6 border-b border-grey-light flex items-center">
                    {path.project ? <Cube size="15" /> : <UserGroup size="15" />}
                    <span className="ml-2">{path.path}</span>
                  </td>
                  <td className="py-4 px-6 border-b border-grey-light text-purple-600 font-bold">{path.channel}</td>
                  <td className="py-4 px-6 border-b border-grey-light">
                    {path.settings !== '' ? (
                      <a className="text-blue-700 hover:underline" href={path.settings} target="_blank">
                        Settings
                      </a>
                    ) : (
                      <span>❌</span>
                    )}
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </Layout>
  )
}

function TableHead({ children }) {
  return <th className="py-4 px-6 text-gray-600 border-b border-grey-light">{children}</th>
}
