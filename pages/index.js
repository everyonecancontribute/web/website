import React, { useState } from 'react'
import Head from 'next/head'
import Layout from '../components/layout'
import { getSortedPostsData } from '../lib/posts'
import Link from 'next/link'
import Date from '../components/date'
import { siteTitle } from '../lib/constants'

export default function Home({ allPostsData }) {
  const [page, setPage] = useState(0)

  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section>
        <div className="text-3xl font-bold mb-5">Blog</div>
        <ul>
          {allPostsData.map(({ id, date, title }) => (
            <li className="mb-7" key={id}>
              <Link href={`/posts/${id}`}>
                <a className="text-2xl font-bold text-purple-800">{title}</a>
              </Link>
              <br />
              <small className="text-gray-600">
                <Date dateString={date} />
              </small>
            </li>
          ))}
        </ul>
      </section>
    </Layout>
  )
}

export async function getStaticProps() {
  const allPostsData = getSortedPostsData()
  return {
    props: {
      allPostsData,
    },
  }
}
