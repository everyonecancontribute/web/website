import axios from 'axios'

export default async (req, res) => {
  try {
    const response = await axios.get(
      `https://gitlab.com/api/v4/groups/8034603/members/all?page=1&per_page=1000&access_token=${process.env.GITLAB_TOKEN}`
    )
    const data = response.data

    res.statusCode = 200
    return res.json(data)
  } catch (err) {
    console.log(err)
    res.statusCode = 500
    return res.json({ status: false })
  }
}
