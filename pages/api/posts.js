import { getRecentPosts } from '../../lib/posts'

export default async (req, res) => {
  res.json(getRecentPosts(3))
}
