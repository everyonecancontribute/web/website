import Layout from '../../components/layout'
import { getAllPostIds, getPostData } from '../../lib/posts'
import Post from '../../components/post'

export default function SinglePost({ postData, authors }) {
  return (
    <Layout>
      <Post title={postData.title} html={postData.contentHtml} date={postData.date} authors={postData.Authors} />
    </Layout>
  )
}

export async function getStaticPaths() {
  const paths = getAllPostIds()
  return {
    paths,
    fallback: false,
  }
}

export async function getStaticProps({ params }) {
  const postData = await getPostData(params.id)
  return {
    props: {
      postData,
    },
  }
}
